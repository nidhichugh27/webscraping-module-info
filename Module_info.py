from selenium import webdriver
from selenium.webdriver.common.by import By
import openpyxl
import os

browser=webdriver.Chrome()
browser.get('')


browser.find_element(By.XPATH, '//*[@id="edit-name"]').send_keys("")
browser.find_element(By.XPATH, '//*[@id="edit-pass"]').send_keys("")
browser.find_element(By.XPATH, '//*[@id="edit-submit"]').click()

browser.get('')

wb=openpyxl.Workbook()
# wb.create_sheet(title="ModuleName")
ws = wb.worksheets[0]
ws.title = "ModuleName"
moduleCount = len(browser.execute_script("return document.getElementsByClassName('module-name table-filter-text-source')"))
for index in range(0, moduleCount):
    moduleName = browser.execute_script(f"return document.getElementsByClassName('module-name table-filter-text-source')[{index}].textContent")
    ws[f"A{index + 1}"] = moduleName
    requirements1 = browser.execute_script(f"return document.getElementsByClassName('requirements')[{index}].textContent")
    ws[f"B{index + 1}"] = requirements1

os.chdir('F:\\')
wb.save('work.xlsx')

browser.close()
